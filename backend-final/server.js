// SERVIDOR QUE VA A CONSUMIR MLAP
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var data = require('./utils/util.js');
var port = process.env.PORT || 3000;
var route = require('./routes/route.js');
var app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(data.urlAppBase,route);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.listen(port);

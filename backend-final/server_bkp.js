/*
  OPERACIONES CRUD SIN MODULARIZAR
*/
// SERVIDOR QUE VA A CONSUMIR MLAP
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var data = require('./utils/util.js');

var port = process.env.PORT || 3000;
var urlAppBase = data.urlAppBase;
var urlMlapRaiz = data.urlMlapRaiz;
var apiKey = data.apiKey;

var app = express();
var clienteMlab;

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.all(urlAppBase + 'users', function(req, res, next) {
    console.log('Siempre pasa por aqui  ...');
   next(); // pass control to the next handler
});

//GET USERS : retorna todos los usuarios de la collection user a traves de mlap
app.get(urlAppBase + 'users', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  console.log("Cliente HTTP MLAP creado");
  var response = {};
  let filter = data.filter.users;
  clienteMlab.get(urlMlapRaiz + '/user?' + filter + apiKey, function(err, resM, body) {
    if (err) {
      response = data.msgError;
    } else {
      response = body.length > 0 ? response = body : response = data.msgEmpty;
    }
    res.status(resM.statusCode);
    res.send(response);
  });
});

//LOGIN de un usuario : agrega un campo logged al documento
app.post(urlAppBase + 'login', function(req, res) {
  var email = req.body.usr_email;
  var password = req.body.usr_password;
  var query = 'q={"usr_email":"' + email + '"}&l=1&';
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { // usuario existe
        let user = body[0];
        if (user.usr_password == password) { //datos validos
          let cambio = data.queryLoged;
          clienteMlab.put(urlMlapRaiz + '/user/' + user._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
            if (!errP) {
              response = {
                "login": "ok",
                "usr_id": user.usr_id,
                "usr_type_id_card": user.usr_type_id_card,
                "usr_id_card": user.usr_id_card,
                "usr_name": user.usr_name,
                "usr_last_name": user.usr_last_name,
                "usr_email": user.usr_email,
                "usr_phone": user.usr_phone
              };
            } else {
              response = data.msgError;
            }
            res.status(resP.statusCode).send(response);
          });
        } else {
          response = data.msgInvalidData;
          res.status(404).send(response);
        }
      } else {
        response = data.msgEmpty;
        res.status(404).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resM.statusCode).send(response);
    }
  });
});

//LOGOUT
app.post(urlAppBase + 'logout', function(req, res) {
  var id = req.body.usr_id;
  var query = 'q={"usr_id":"' + id + '", "logged":true}&l=1&';
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length == 1) {
        let user = body[0];
        let cambio = data.queryLogout;
        clienteMlab.put(urlMlapRaiz + '/user/' + user._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          if (!errP) {
            response = {
              "logout": "ok",
              "usr_id": user.usr_id,
              "usr_name": user.usr_name
            };
          } else {
            response = data.msgError;
          }
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgUserNotLoged;
        res.status(200).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resM.statusCode).send(response);
    }
  });
});

//GET USER by ID : Return user with id
app.get(urlAppBase + 'users/:id', function(req, res) {
  var id = req.params.id;
  var query = 'q={"usr_id":"' + id + '"}&f={"_id":0}&';
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(err, resM, body) {
    if (!err) {
      response = body.length > 0 ? body[0] : data.msgEmpty;
    } else {
      response = data.msgError;
    }
    res.status(resM.statusCode);
    res.send(response);
  });
});

//POST - Create new user
app.post(urlAppBase + 'users', function(req, res) {
  console.log(req.body);
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let id = req.body.usr_id == null ? req.body.usr_id_card : req.body.usr_id;
  let response = {};
  let query = 'q={"usr_id":"' + id + '"}&';
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length == 0) {
        let newUser = {
          "usr_id": req.body.usr_id_card,
          "usr_type_id_card": req.body.usr_type_id_card,
          "usr_id_card": req.body.usr_id_card,
          "usr_name": req.body.usr_name,
          "usr_last_name": req.body.usr_last_name,
          "usr_email": req.body.usr_email,
          "usr_password": req.body.usr_password,
          "usr_phone": req.body.usr_phone,
          "usr_ip_address": req.body.usr_ip_address
        };
        clienteMlab.post(urlMlapRaiz + '/user?' + apiKey, newUser, function(errP, resP, bodyP) {
          if (!errP) {
            response = {
              "msg": "Usuario agregado correctamente",
              "usr_id": req.body.usr_id
            };
          } else {
            response = data.msgError;
          }
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.existUser;
        res.status(data.httpCodes.registered).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

//PUT of user - tutor
app.put(urlAppBase + 'users/:id', function(req, res) {
  let id = req.params.id;
  let query = 'q={"usr_id":"' + id + '"}&';
  let cambio;
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgSuccessModify;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgEmpty;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

//DELETE user with id: string
app.delete(urlAppBase + 'users/:id', function(req, res) {
  let id = req.params.id;
  let query = 'q={"usr_id":"' + id + '"}&';
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        clienteMlab.delete(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, function(errD, resD, bodyD) {
          console.log(bodyD);
          response = errD ? data.msgError : data.msgSuccessDelete;
          res.status(resD.statusCode).send(response);
        });
      } else {
        response = data.msgEmpty;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

/* OPERACIONES BÁSICAS - CUENTAS
===============================================================================================================================
===============================================================================================================================
*/
//mlap https://api.mlab.com/api/1/databases/bdapiperu/collections/account?q={userID:2}&apiKey=ILQ4EvViJQjbaLiY3Z8526GHW-eITWa_
//Get all accounts
app.get(urlAppBase + 'accounts/', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  let filter = data.filter.accounts;
  clienteMlab.get(urlMlapRaiz + '/user?' + filter + apiKey, function(errG, resG, bodyG) {
    if (errG) {
      response = data.msgError;
    } else {
      response = bodyG.length > 0 ? bodyG : data.msgEmpty;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
});

// get accounts by id_user
app.get(urlAppBase + 'users/:id/accounts', function(req, res) {
  let id = req.params.id;
  let query = 'q={"usr_id":"' + id + '"}&' + data.filter.accounts;
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      response = bodyG.length > 0 ? bodyG[0] : data.msgEmpty;
    } else {
      response = data.msgError;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
});
//create new account : Adiciona un nuevo elemento cuenta en la ultima posiciòn del vector accounts
app.post(urlAppBase + 'users/:id/accounts', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let id = req.params.id;
  let response = {};
  let query = 'q={"usr_id":"' + id + '"}&';
  let cambio;

  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) { //validar si existe usuario y cuenta --modificar query
    if (!errG) {
      if (bodyG.length > 0) { //implementar logica para autogenerar el numero de cuenta
        let registro = bodyG[0];
        let indexAccount = bodyG[0].accounts == null ? 0 : bodyG[0].accounts.length;
        let newAccount = {
          "acc_id": req.body.acc_id,
          "acc_entity": req.body.acc_entity,
          "acc_office": req.body.acc_office,
          "acc_check_digit": req.body.acc_check_digit,
          "acc_type": req.body.acc_type,
          "acc_number": req.body.acc_number,
          "acc_complete": req.body.acc_complete,
          "acc_estatus": req.body.acc_estatus,
          "acc_divisa": req.body.acc_divisa,
          "acc_dispo_mount": req.body.acc_dispo_mount,
          "acc_cont_mount": req.body.acc_cont_mount
        };
        if (indexAccount == 0) registro.accounts = [];
        registro.accounts[indexAccount] = newAccount;

        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgAddAccountSuccess;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

//MODIFICAR CUENTA -- Se debe enviar todos los datos de la cuenta
app.put(urlAppBase + 'users/:idUser/accounts/:idAccount', function(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let response = {};
  let query = 'q={"usr_id":"' + idUser + '" , "accounts.acc_id":"' + idAccount + '"  }&';
  let cambio;
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) { //validar si existe usuario y cuenta --modificar query
    if (!errG) {
      console.log(query);
      if (bodyG.length > 0) { //implementar logica para autogenerar el numero de cuenta
        let registro = bodyG[0];
        for (i in registro.accounts) {
          if (registro.accounts[i].acc_id == idAccount) {
            registro.accounts[i] = req.body;
          }
        }
        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgSuccessModify;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

//ELIMINAR CUENTA DE USUARIO .- reemplaza por null o bederìa cambiarle el estado
app.delete(urlAppBase + 'users/:idUser/accounts/:idAccount', function(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let response = {};
  let query = 'q={"usr_id":"' + idUser + '" , "accounts.acc_id" :' + idAccount + '}&';
  let cambio;
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        let registro = bodyG[0];
        console.log(registro);
        for (i in registro.accounts) {
          if (registro.accounts[i] != null && registro.accounts[i].acc_id == idAccount) {
            delete registro.accounts[i]; //null el campo ...  no deja iterar a menos que ....
          }
        }
        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgSuccessDelete;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

/* OPERACIONES CRUD - MOVEMENTS
===============================================================================================================================
===============================================================================================================================
*/
// GET MOVEMENTS BY USER - ACCOUNT
app.get(urlAppBase + 'users/:idUser/accounts/:idAccount/movements', function(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  var query = 'q={"usr_id":"' + idUser + '", "accounts.acc_complete":"' + idAccount + '"}&f={"accounts":1}&';
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        for (i in bodyG[0].accounts) {
          if (bodyG[0].accounts[i].acc_complete == idAccount) {
            response.account = bodyG[0].accounts[i].acc_complete;
            response.movements = bodyG[0].accounts[i].movements == null ? [] : bodyG[0].accounts[i].movements;
            break;
          }
        }
      } else {
        response = data.msgEmpty;
      }
    } else {
      response = data.msgError;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
});

//POST MOVEMENTS - NUEVO MOVIMIENTO T-T
app.post(urlAppBase + 'users/:idUser/accounts/:idAccount/movements', function(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let query = 'q={"usr_id":"' + idUser + '", "accounts.acc_complete":"' + idAccount + '"}&f={"accounts":1}&'; //retorna el array de cuentas asociadas al cliente con idUser
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        for (i in bodyG[0].accounts) {
          if (bodyG[0].accounts[i].acc_complete == idAccount) {
            let indexMovements = bodyG[0].accounts[i].movements == null ? 0 : bodyG[0].accounts[i].movements.length;
            if (indexMovements == 0) bodyG[0].accounts[i].movements = [];
            bodyG[0].accounts[i].movements[indexMovements] = req.body.movements;
            let cambio = '{"$set":' + JSON.stringify(bodyG[0]) + '}';
            clienteMlab = requestJson.createClient(urlMlapRaiz);
            clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              response = errP ? data.msgError : data.msgAddMovementSuccess;
              res.status(resP.statusCode).send(response);
            });
            break;
            }
          }
      } else {
        response = data.msgEmpty;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
});

app.listen(port);

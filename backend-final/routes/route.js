const express = require('express');
const accessController = require('../controllers/accessController.js');
const usersController = require('../controllers/usersController.js');
const accountsController = require('../controllers/accountsController.js');
const movementsController = require('../controllers/movementsController.js');
const auth = require('../middleware/authProcess.js');

const app = express.Router();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

//LOGIN
app.post('/login', accessController.login);
//LOGOUT
app.post('/logout', accessController.logout);
//AUTHENTICATE
app.post('/authenticate', accessController.authenticate);

/* OPERACIONES BÁSICAS - USUARIOS
===============================================================================================================================
===============================================================================================================================
*/
//GET USERS : retorna todos los usuarios de la collection user a traves de mlap
app.get('/users', auth.isAuth, usersController.getUsers);
//GET USER by ID : Return user with id
app.get('/users/:id', auth.isAuth, usersController.getUser);
//POST - Create new user
app.post('/users', usersController.createUser);
//PUT of user - tutor
app.put('/users/:id', auth.isAuth, usersController.updateUser);
//DELETE user with id: string
app.delete('/users/:id', auth.isAuth, usersController.deleteUser);

/* OPERACIONES BÁSICAS - CUENTAS
===============================================================================================================================
===============================================================================================================================
*/
//Get all accounts
app.get('/accounts', auth.isAuth, accountsController.getAccounts);
// get accounts by id_user
app.get('/users/:id/accounts', auth.isAuth, accountsController.getAccount);
//create new account : Adiciona un nuevo elemento cuenta en la ultima posiciòn del vector accounts
app.post('/users/:id/accounts', auth.isAuth, accountsController.createAccount);
//MODIFICAR CUENTA -- Se debe enviar todos los datos de la cuenta
app.put('/users/:idUser/accounts/:idAccount', auth.isAuth, accountsController.updateAccount);
//ELIMINAR CUENTA DE USUARIO .- reemplaza por null o bederìa cambiarle el estado
app.delete('/users/:idUser/accounts/:idAccount', auth.isAuth, accountsController.deleteAccount);
/* OPERACIONES CRUD - MOVEMENTS
===============================================================================================================================
===============================================================================================================================
*/
// GET MOVEMENTS BY USER - ACCOUNT
app.get('/users/:idUser/accounts/:idAccount/movements', auth.isAuth, movementsController.getMovements);
//POST MOVEMENTS - NUEVO MOVIMIENTO
app.post('/users/:idUser/accounts/:idAccount/movements', auth.isAuth, movementsController.createMovement);

module.exports = app;

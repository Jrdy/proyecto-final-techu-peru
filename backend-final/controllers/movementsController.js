var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var data = require('../utils/util.js');

var urlMlapRaiz = data.urlMlapRaiz;
var apiKey = data.apiKey;

var app = express();
var clienteMlab;

/**
 * Access Controller .
 * @module controller/movementsController
 * @see controller:movementsController
 */

// GET MOVEMENTS BY USER - ACCOUNT
/**
 * Movements Controller - getMovements : Retorna el listado de los movimientos de la cuenta (idAccount) del usuario (idUser).
 * @param {string} idUser Identificador del usuario.
 * @param {string} idAccount Identificador de la cuenta
 * @returns {string} Retorna Lista de movimientos de la cuenta (idAccount) del usuario con idUser
 */
function getMovements(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  var query = 'q={"usr_id":"' + idUser + '", "accounts.acc_complete":"' + idAccount + '"}&f={"accounts":1}&';
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        for (i in bodyG[0].accounts) {
          if (bodyG[0].accounts[i].acc_complete == idAccount) {
            response.account = bodyG[0].accounts[i].acc_complete;
            response.movements = bodyG[0].accounts[i].movements == null ? [] : bodyG[0].accounts[i].movements;
            break;
          }
        }
      } else {
        response = data.msgEmpty;
      }
    } else {
      response = data.msgError;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
}

//POST MOVEMENTS - NUEVO MOVIMIENTO
/**
 * Movements Controller - postMovements :Añade un movimiento en la última posición de movimientos de la cuenta, si es una cuenta sin movimientos lo pone en la primera posición
 * @param {string} idUser Identificador del usuario.
 * @param {string} idAccount Identificador de la cuenta
 * @param {string} accounts Detalle de la cuenta
 * @returns {string} Retorna mensaje de confirmación o error.
 */
function createMovement(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let query = 'q={"usr_id":"' + idUser + '", "accounts.acc_complete":"' + idAccount + '"}&f={"accounts":1}&'; //retorna el array de cuentas asociadas al cliente con idUser
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        for (i in bodyG[0].accounts) {
          if (bodyG[0].accounts[i].acc_complete == idAccount) {
            let indexMovements = bodyG[0].accounts[i].movements == null ? 0 : bodyG[0].accounts[i].movements.length;
            if (indexMovements == 0) bodyG[0].accounts[i].movements = [];
            bodyG[0].accounts[i].movements[indexMovements] = req.body.movements;
            let cambio = '{"$set":' + JSON.stringify(bodyG[0]) + '}';
            clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              response = errP ? data.msgError : data.msgAddMovementSuccess;
              res.status(resP.statusCode).send(response);
            });
            break;
            }
          }
      } else {
        response = data.msgEmpty;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
}

module.exports={
  getMovements,
  createMovement
};

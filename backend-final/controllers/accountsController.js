var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var data = require('../utils/util.js');

var urlMlapRaiz = data.urlMlapRaiz;
var apiKey = data.apiKey;

var app = express();
var clienteMlab;

/**
 * Access Controller .
 * @module controller/accountsController
 * @see controller:accountsController
 */

/**
 * Accounts Controller - getAccounts : Retorna el listado de cuentas de cada usuario
 * @returns {string} Retorna Lista de cuentas por cada usuario registrado en la collection.
 */
function getAccounts(req, res) {
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  let filter = data.filter.accounts;
  clienteMlab.get(urlMlapRaiz + '/user?' + filter + apiKey, function(errG, resG, bodyG) {
    if (errG) {
      response = data.msgError;
    } else {
      response = bodyG.length > 0 ? bodyG : data.msgEmpty;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
}
// get accounts by id_user
/**
 * Accounts Controller - getAccount : Retorna el detalle de la cuenta para el usuario con ID
 * @param {string} id Identificador del usuario
 * @param {string} idAccount Identificador de la cuenta
 * @returns {string} Retorna Lista de cuentas del usuario con ID
 */
function getAccount(req, res) {
  let id = req.params.id;
  let query = 'q={"usr_id":"' + id + '"}&' + data.filter.accounts;
  let response = {};
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      response = bodyG.length > 0 ? bodyG[0] : data.msgEmpty;
    } else {
      response = data.msgError;
    }
    res.status(resG.statusCode);
    res.send(response);
  });
}
//create new account : Adiciona un nuevo elemento cuenta en la ultima posiciòn del vector accounts
/**
 * Accounts Controller - createAccount : Crea un nuevo usuario
 * @param {string} id Identificador del usuario
 * @param {string} body JSON con los datos de la nueva cuenta
 * @returns {string} Retorna mensaje de cofirmación
 */
function createAccount(req, res) {
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let id = req.params.id;
  let response = {};
  let query = 'q={"usr_id":"' + id + '"}&';
  let cambio;
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) { //validar si existe usuario y cuenta --modificar query
    if (!errG) {
      if (bodyG.length > 0) { //implementar logica para autogenerar el numero de cuenta
        let registro = bodyG[0];
        let indexAccount = bodyG[0].accounts == null ? 0 : bodyG[0].accounts.length;
        let newAccount = {
          "acc_id": req.body.acc_id,
          "acc_entity": req.body.acc_entity,
          "acc_office": req.body.acc_office,
          "acc_check_digit": req.body.acc_check_digit,
          "acc_type": req.body.acc_type,
          "acc_number": req.body.acc_number,
          "acc_complete": req.body.acc_complete,
          "acc_estatus": req.body.acc_estatus,
          "acc_divisa": req.body.acc_divisa,
          "acc_dispo_mount": req.body.acc_dispo_mount,
          "acc_cont_mount": req.body.acc_cont_mount
        };
        if (indexAccount == 0) registro.accounts = [];
        registro.accounts[indexAccount] = newAccount;

        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgAddAccountSuccess;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
}

//MODIFICAR CUENTA -- Se debe enviar todos los datos de la cuenta.
/**
 * Accounts Controller - updateAccount : Modifica la cuenta con ID del usuario con IDUser
 * @param {string} idUser Identificador del usuario
 * @param {string} idAccount Identificador de la cuenta
 * @param {string} body JSON con los datos de actualizados
 * @returns {string} Retorna mensaje de cofirmación o error
 */
function updateAccount(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let response = {};
  let query = 'q={"usr_id":"' + idUser + '" , "accounts.acc_id":"' + idAccount + '"  }&';
  let cambio;
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) { //validar si existe usuario y cuenta --modificar query
    if (!errG) {
      console.log(query);
      if (bodyG.length > 0) { //implementar logica para autogenerar el numero de cuenta
        let registro = bodyG[0];
        for (i in registro.accounts) {
          if (registro.accounts[i].acc_id == idAccount) {
            registro.accounts[i] = req.body;
          }
        }
        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgSuccessModify;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
}

//ELIMINAR CUENTA DE USUARIO .- reemplaza por null o bederìa cambiarle el estado
/**
 * Accounts Controllers - deleteAccount : Elimina la cuenta con ID del usuario con IDUser
 * @param {string} idUser Identificador del usuario
 * @param {string} idAccount ID de la cuenta a eliminar
 * @returns {string} Retorna mensaje de cofirmación o error
 */
function deleteAccount(req, res) {
  let idUser = req.params.idUser;
  let idAccount = req.params.idAccount;
  let response = {};
  let query = 'q={"usr_id":"' + idUser + '" , "accounts.acc_id" :' + idAccount + '}&';
  let cambio;
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  clienteMlab.get(urlMlapRaiz + '/user?' + query + apiKey, function(errG, resG, bodyG) {
    if (!errG) {
      if (bodyG.length > 0) {
        let registro = bodyG[0];
        console.log(registro);
        for (i in registro.accounts) {
          if (registro.accounts[i] != null && registro.accounts[i].acc_id == idAccount) {
            delete registro.accounts[i]; //null el campo ...  no deja iterar a menos que ....
          }
        }
        cambio = '{"$set":' + JSON.stringify(registro) + '}';
        clienteMlab.put(urlMlapRaiz + '/user/' + bodyG[0]._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          response = errP ? data.msgError : data.msgSuccessDelete;
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgInvalidData;
        res.status(resG.statusCode).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resG.statusCode).send(response);
    }
  });
}

module.exports = {
  getAccounts,
  getAccount,
  createAccount,
  updateAccount,
  deleteAccount
};

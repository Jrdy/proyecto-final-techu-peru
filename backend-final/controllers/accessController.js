var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var data = require('../utils/util.js');
var security = require('../security/validations.js');

var urlMlapRaiz = data.urlMlapRaiz;
var apiKey = data.apiKey;

var app = express();
var clienteMlab;

/**
 * Access Controller .
 * @module controller/accessController
 * @see controller:accessController
 */

//LOGIN de un usuario : agrega un campo logged al documento
/**
 * Access Controller - Login
 * @param {string} usr_email correo del usuario.
 * @param {string} usr_password contraseña del usuario.
 * @returns {string} Retorna datos del usuario logeado con el token de seguridad para invocar los demás servicios
 */
function login(req, res) {
  var email = req.body.usr_email;
  var password = req.body.usr_password;
  var query = 'q={"usr_email":"' + email + '"}&l=1&';
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { // usuario existe
        let user = body[0];
        if (user.usr_password == password) { //datos validos
          let cambio = data.queryLoged;
          clienteMlab.put(urlMlapRaiz + '/user/' + user._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
            if (!errP) {
              let token = security.createToken(user.usr_id);
              response = {
                "login": "ok",
                "usr_id": user.usr_id,
                "usr_type_id_card": user.usr_type_id_card,
                "usr_id_card": user.usr_id_card,
                "usr_name": user.usr_name,
                "usr_last_name": user.usr_last_name,
                "usr_email": user.usr_email,
                "usr_phone": user.usr_phone,
                "token": token,
              };
            } else {
              response = data.msgError;
            }
            res.status(resP.statusCode).send(response);
          });
        } else {
          response = data.msgInvalidData;
          res.status(404).send(response);
        }
      } else {
        response = data.msgEmpty;
        res.status(404).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resM.statusCode).send(response);
    }
  });
}

//LOGOUT
/**
 * Access Controller - Logout
 * @param {string} usr_id ID del usuario logado
 * @returns {string} Retorna un JSON de confirmación
 */
function logout(req, res) {
  var id = req.body.usr_id;
  var query = 'q={"usr_id":"' + id + '", "logged":true}&l=1&';
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length == 1) {
        let user = body[0];
        let cambio = data.queryLogout;
        clienteMlab.put(urlMlapRaiz + '/user/' + user._id.$oid + '?' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          if (!errP) {
            response = {
              "logout": "ok",
              "usr_id": user.usr_id,
              "usr_name": user.usr_name
            };
          } else {
            response = data.msgError;
          }
          res.status(resP.statusCode).send(response);
        });
      } else {
        response = data.msgUserNotLoged;
        res.status(200).send(response);
      }
    } else {
      response = data.msgError;
      res.status(resM.statusCode).send(response);
    }
  });
}

//Authenticate
/**
 * Access Controller - Authenticate
 * @param {string} usr_id ID del usuario (correo electrónico).
 * @param {string} usr_password contraseña del usuario.
 * @returns {string} Valida si el password corresponde al usuario
 */
function authenticate(req, res) {
  var email = req.body.usr_email;
  var password = req.body.usr_password;
  var query = 'q={"usr_email":"' + email + '", "usr_password":"' + password + '"}&l=1&';
  clienteMlab = requestJson.createClient(urlMlapRaiz);
  let response = {};
  clienteMlab.get(urlMlapRaiz + "/user?" + query + apiKey, function(err, resM, body) {
    if (!err) {
      response = body.length == 1 ? data.authSucess : data.msgInvalidData;
      res.status(resM.statusCode).send(response);
    } else {
      response = data.msgError;
      res.status(resM.statusCode).send(response);
    }
  });
}

module.exports = {
  login,
  logout,
  authenticate
};

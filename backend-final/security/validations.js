const jwt=require('jwt-simple');
const moment=require('moment');
const util = require('../utils/util.js');

//Crea un token con el id del usuario
function createToken(idUser) {
  var payload = {
    sub: idUser,
    iat: moment().unix(),
    iat: moment().add(14, 'days').unix(),
  };
  return jwt.encode(payload, util.secret_token);
}

function decodeToken(token){
    const decode=new Promise((resolve,reject)=>{
    try{
      const payload = jwt.decode(token,util.secret_token);
      if(payload.exp < moment().unix){
      reject({
        status:401,
        message: 'Token ha Expirado'
      })
      }
      resolve(payload.sub);
    }catch (err){
      reject({
        status:500,
        message: 'Invalid token'
      })
    }
});
return decode;
}

module.exports={
  createToken,
  decodeToken
};
